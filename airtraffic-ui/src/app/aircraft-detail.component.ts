import {Component, Input} from '@angular/core';
import {Aircraft} from "./aircraft";

@Component({
  selector: 'aircraft-detail',
  templateUrl: './aircraft-detail.component.html',
})
export class AircraftDetailComponent {

  @Input() aircraft: Aircraft

}
