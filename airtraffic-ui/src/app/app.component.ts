import { Component } from '@angular/core';
import {Aircraft} from "./aircraft";
import {DepartureService} from "./departure.service";
import {DepartingAircraft} from "./departing-aircraft";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DepartureService]
})
export class AppComponent {
  title = 'Air Traffic Controller';

  nextDeparture: Aircraft;
  departingAircraftList: DepartingAircraft[];

  constructor(private departureService: DepartureService) { }

  flightDeparted(event) {
    this.departureService.flightDeparted().then(response => this.loadHomePage());
  }

  clearAllDepartures(event) {
    this.departureService.clearDepartures().then(response => this.loadHomePage());
  }

  loadHomePage(): void {
    this.getAllDepartures();
    this.getDepartingAircraft();
  }

  getDepartingAircraft(): void {
    this.departureService.getDepartingAircraft().then(aircraft => this.nextDeparture = aircraft);
  }

  getAllDepartures(): void {
    this.departureService.getAllDepartures().then(list => this.departingAircraftList = list);
  }

  ngOnInit(): void {
    this.loadHomePage();
  }
}
