import { Injectable } from '@angular/core';
import {Aircraft} from "./aircraft";
import {Headers, Http} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import {DepartingAircraft} from "./departing-aircraft";

@Injectable()
export class DepartureService {

  departingAircraftUrl = '/api/queue/departure/aircraft';
  allDepartureUrl = '/api/queue/departure';
  constructor(private http: Http) {}

  getDepartingAircraft(): Promise<Aircraft> {
    return this.http.get(this.departingAircraftUrl)
      .toPromise().then(response => response.json() as Aircraft)
      .catch(this.handleError)
  }

  getAllDepartures(): Promise<DepartingAircraft[]> {
    return this.http.get(this.allDepartureUrl)
      .toPromise().then(response => response.json() as DepartingAircraft[])
    .catch(this.handleError)
  }

  flightDeparted(): Promise<Aircraft> {
    return this.http.delete(this.departingAircraftUrl)
      .toPromise().then(response => response.json() as Aircraft)
      .catch(this.handleError)
  }

  addDepartingAircraft(aircraft): Promise<any> {
    console.info("adding aircraft"+(aircraft as Aircraft));
    return this.http.post(this.departingAircraftUrl, aircraft)
      .toPromise().then(response => console.log(response))
      .catch(this.handleError)
  }

  clearDepartures(): Promise<any> {
    console.info("clearing");
    return this.http.delete(this.allDepartureUrl).toPromise().then(response => console.log("cleared all departures"))
      .catch(this.handleError)
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
