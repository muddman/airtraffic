import { Component } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {DepartureService} from "./departure.service";
import {AppModule} from "./app.module";
import {AppComponent} from "./app.component";


@Component({
  selector: 'aircraft-form',
  templateUrl: './aircraft-form.component.html',
  providers: [DepartureService]
})
export class AircraftFormComponent {

  types = ["PASSENGER", "CARGO"];
  sizes = ["LARGE", "SMALL"];

  public aircraftForm = this.fb.group({
    name: ["", Validators.required],
    type: ["", Validators.required],
    size: ["", Validators.required],
  });

  constructor(public fb: FormBuilder, private departureService: DepartureService, private appComponent:AppComponent) {}

  doAddDeparture(event) {
    this.departureService.addDepartingAircraft(this.aircraftForm.value)
      .then(response => this.appComponent.loadHomePage());
  }

}
