import {Aircraft} from "./aircraft";

export class DepartingAircraft {
  aircraft: Aircraft;
  queueTime: string;
}
