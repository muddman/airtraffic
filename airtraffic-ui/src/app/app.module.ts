import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AircraftDetailComponent} from "./aircraft-detail.component";
import {HttpModule} from "@angular/http";
import {DepartureQueueComponent} from "./departure-queue.component";
import {AircraftFormComponent} from "./aircraft-form.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    AircraftDetailComponent,
    DepartureQueueComponent,
    AircraftFormComponent
  ],
  imports: [
    BrowserModule, HttpModule, FormsModule, ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
