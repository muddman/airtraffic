import {Component, Input} from '@angular/core';
import {DepartingAircraft} from "./departing-aircraft";

@Component({
  selector: 'departure-queue-detail',
  templateUrl: './departure-queue.component.html',
})
export class DepartureQueueComponent {

  @Input() departingAircraftList: DepartingAircraft[]

}
