package com.muddworks.airtraffic.service;

import com.muddworks.airtraffic.domain.Aircraft;
import com.muddworks.airtraffic.domain.DepartingAircraft;
import com.muddworks.airtraffic.domain.QueueManager;
import com.muddworks.airtraffic.domain.exceptions.EmptyQueueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

/**
 * Created on 10/7/17.
 */
@Service
public class AirCraftQueueManagerService {

    private Comparator<DepartingAircraft> comparator;

    private QueueManager<DepartingAircraft> departureQueue;

    @Autowired
    public AirCraftQueueManagerService(Comparator<DepartingAircraft> comparator) {
        this.comparator = comparator;
        departureQueue = new QueueManager<>("departures", comparator);
    }

    public void addToQueue(Aircraft aircraft) {
        departureQueue.add(new DepartingAircraft(aircraft));
    }

    public Aircraft peekQueue() {
        DepartingAircraft departingAircraft = departureQueue.peek();
        if(departingAircraft!=null) {
            return departingAircraft.getAircraft();
        } else {
            throw new EmptyQueueException("Departures queue is empty");
        }
    }

    public Aircraft popQueue() {
        DepartingAircraft departingAircraft = departureQueue.pop();
        if(departingAircraft!=null) {
            return departingAircraft.getAircraft();
        } else {
            throw new EmptyQueueException("Departures queue is empty");
        }
    }

    public void resetQueue() {
        departureQueue.clear();
    }

    public List<DepartingAircraft> getAircraft() {
        return departureQueue.toList();
    }
}
