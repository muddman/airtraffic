package com.muddworks.airtraffic.domain;

import org.springframework.stereotype.Component;

import java.util.Comparator;

/**
 * Created on 10/8/17.
 */
@Component
public class DepartingAircraftComparator implements Comparator<DepartingAircraft> {

    @Override
    public int compare(DepartingAircraft first, DepartingAircraft second) {
        int aircraftComparison = first.getAircraft().compareTo(second.getAircraft());
        if (aircraftComparison == 0) {
            return first.getQueueTime().compareTo(second.getQueueTime());
        }
        return aircraftComparison;
    }
}
