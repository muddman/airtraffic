package com.muddworks.airtraffic.domain;

import com.google.common.base.Objects;

/**
 * Created on 10/7/17.
 */
public class Aircraft implements Comparable<Aircraft> {

    private String name;
    private AircraftType type;
    private AircraftSize size;

    protected Aircraft() {
    }

    public Aircraft(String name, AircraftType type, AircraftSize size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public AircraftType getType() {
        return type;
    }

    public AircraftSize getSize() {
        return size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aircraft aircraft = (Aircraft) o;
        return Objects.equal(name, aircraft.name) &&
                Objects.equal(type, aircraft.type) &&
                Objects.equal(size, aircraft.size);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, type, size);
    }

    @Override
    public int compareTo(Aircraft aircraft) {
        int typeComparison = getType().compareTo(aircraft.getType());
        if(typeComparison != 0) {
            return typeComparison * -1;
        }

        int sizeComparison = getSize().compareTo(aircraft.getSize());
        if(sizeComparison != 0) {
            return sizeComparison * -1;
        }

        return 0;
    }
}
