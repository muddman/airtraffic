package com.muddworks.airtraffic.domain;

/**
 * Order here is important.  Aircraft that are first off the runway should appear first. Aircraft that are
 * the last off the runway should appear last.
 *
 * Created on 10/7/17.
 */
public enum AircraftSize {
    SMALL, LARGE
}
