package com.muddworks.airtraffic.domain;

import com.google.common.collect.Lists;

import java.util.*;

/**
 * Created on 10/7/17.
 */
public class QueueManager<T> {

    private Queue<T> queue;
    private String queueName;
    private Comparator<T> comparator;

    public QueueManager(String queueName) {
        this.queueName = queueName;
        queue = new PriorityQueue<>();
    }

    public QueueManager(String queueName, Comparator<T> comparator) {
        this.queueName = queueName;
        this.comparator = comparator;
        queue = new PriorityQueue<>(comparator);
    }

    public String getQueueName() {
        return queueName;
    }

    public void add(T item) {
        queue.add(item);
    }

    public T peek() {
        return queue.peek();
    }

    public T pop() {
        return queue.poll();
    }

    public void clear() {
        queue.clear();
    }

    public List<T> toList() {
        List<T> results = Lists.newArrayList(queue.iterator());
        results.sort(comparator);
        return results;
    }
}
