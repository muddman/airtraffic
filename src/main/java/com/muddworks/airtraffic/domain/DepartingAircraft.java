package com.muddworks.airtraffic.domain;

import com.google.common.base.Objects;

import java.time.LocalDateTime;

/**
 * Created on 10/8/17.
 */
public class DepartingAircraft {

    private LocalDateTime queueTime;
    private Aircraft aircraft;

    public DepartingAircraft(Aircraft aircraft) {
        this.queueTime = LocalDateTime.now();
        this.aircraft = aircraft;
    }

    public LocalDateTime getQueueTime() {
        return queueTime;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartingAircraft that = (DepartingAircraft) o;
        return Objects.equal(queueTime, that.queueTime) &&
                Objects.equal(aircraft, that.aircraft);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(queueTime, aircraft);
    }
}
