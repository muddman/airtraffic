package com.muddworks.airtraffic.domain.exceptions;

/**
 * Created on 10/8/17.
 */
public class EmptyQueueException extends RuntimeException {

    public EmptyQueueException(String message) {
        super(message);
    }
}
