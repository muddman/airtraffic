package com.muddworks.airtraffic.api;

import com.muddworks.airtraffic.domain.Aircraft;
import com.muddworks.airtraffic.service.AirCraftQueueManagerService;
import com.muddworks.airtraffic.domain.DepartingAircraft;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created on 10/7/17.
 */
@RestController
@RequestMapping("/api/queue/departure")
public class AirCraftQueueManagerResource {

    private static Logger logger = LoggerFactory.getLogger(AirCraftQueueManagerResource.class);

    private AirCraftQueueManagerService service;

    @Autowired
    public AirCraftQueueManagerResource(AirCraftQueueManagerService service) {
        this.service = service;
    }

    @PostMapping("/aircraft")
    public void addAircraft(@RequestBody Aircraft aircraft) {
        logger.debug("Adding aircraft to departure queue");
        service.addToQueue(aircraft);
    }

    @GetMapping("/aircraft")
    public Aircraft peekAircraft() {
        logger.debug("Peeking aircraft on departure queue");
        return service.peekQueue();
    }

    @DeleteMapping("/aircraft")
    public Aircraft popAircraft() {
        logger.debug("Popping aircraft on departure queue");
        return service.popQueue();
    }

    @GetMapping
    public List<DepartingAircraft> getAircraft() {
        logger.debug("returning aircraft");
        return service.getAircraft();
    }

    @DeleteMapping
    public void clearDepartureQueue() {
        logger.debug("resetting departure queue");
        service.resetQueue();
    }
}
