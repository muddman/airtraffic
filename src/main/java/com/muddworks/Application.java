package com.muddworks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created on 10/7/17.
 */
@SpringBootApplication(scanBasePackages = "com.muddworks")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
