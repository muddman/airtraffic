package com.muddworks.support.api;

import com.google.common.collect.ImmutableMap;
import com.muddworks.airtraffic.domain.exceptions.EmptyQueueException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Map;

/**
 * Created on 4/1/17.
 */
@ControllerAdvice
public class GlobalResourceExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody Map<String, String> defaultHandler() {
        return ImmutableMap.of("message", "An error occurred");
    }

    @ExceptionHandler(EmptyQueueException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody Map<String, String> emptyQueueExceptionHandler(EmptyQueueException e) {
        return ImmutableMap.of("message", e.getMessage());
    }
}
