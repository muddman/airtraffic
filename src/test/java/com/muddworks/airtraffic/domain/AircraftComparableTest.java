package com.muddworks.airtraffic.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created on 10/7/17.
 */
@RunWith(Parameterized.class)
public class AircraftComparableTest {

    private AircraftSize firstSize;
    private AircraftSize secondSize;
    private AircraftType firstType;
    private AircraftType secondType;
    private int expectedResult;

    public AircraftComparableTest(AircraftType firstType, AircraftSize firstSize,
                                  AircraftType secondType, AircraftSize secondSize,
                                  int expectedResult) {
        this.firstSize = firstSize;
        this.secondSize = secondSize;
        this.firstType = firstType;
        this.secondType = secondType;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {AircraftType.PASSENGER, AircraftSize.LARGE, AircraftType.PASSENGER, AircraftSize.SMALL, -1},
                {AircraftType.PASSENGER, AircraftSize.LARGE, AircraftType.PASSENGER, AircraftSize.LARGE, 0},
                {AircraftType.PASSENGER, AircraftSize.SMALL, AircraftType.PASSENGER, AircraftSize.LARGE, 1},
                {AircraftType.CARGO, AircraftSize.LARGE, AircraftType.PASSENGER, AircraftSize.LARGE, 1},
                {AircraftType.CARGO, AircraftSize.SMALL, AircraftType.PASSENGER, AircraftSize.LARGE, 1},
                {AircraftType.CARGO, AircraftSize.SMALL, AircraftType.CARGO, AircraftSize.SMALL, 0},
        });
    }

    @Test
    @Parameterized.Parameters
    public void testCompareTo() {

        Aircraft firstAirCraft = new Aircraft("N1234", firstType, firstSize);
        Aircraft secondAirCraft = new Aircraft("N1234", secondType, secondSize);

        assertThat(firstAirCraft.compareTo(secondAirCraft), is(expectedResult));
    }



}
