package com.muddworks.airtraffic.domain;

import org.junit.Test;

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created on 10/7/17.
 */
public class AircraftTest {

    @Test
    public void testConstructor() {
        Aircraft aircraft = new Aircraft("N1234", AircraftType.PASSENGER, AircraftSize.SMALL);
        assertThat(aircraft.getName(), is("N1234"));
        assertThat(aircraft.getSize(), is(AircraftSize.SMALL));
        assertThat(aircraft.getType(), is(AircraftType.PASSENGER));
    }

    @Test
    public void testOrder() {

        Queue<Aircraft> sortedCraft = new PriorityQueue<>();
        sortedCraft.add(new Aircraft("N1234", AircraftType.PASSENGER, AircraftSize.SMALL));
        sortedCraft.add(new Aircraft("N1234", AircraftType.CARGO, AircraftSize.SMALL));
        sortedCraft.add(new Aircraft("N1234", AircraftType.CARGO, AircraftSize.LARGE));
        sortedCraft.add(new Aircraft("N1234", AircraftType.PASSENGER, AircraftSize.LARGE));
        sortedCraft.add(new Aircraft("N1234", AircraftType.PASSENGER, AircraftSize.SMALL));

        Aircraft aircraft = sortedCraft.poll();

        assertThat(aircraft.getType(), is(AircraftType.PASSENGER));
        assertThat(aircraft.getSize(), is(AircraftSize.LARGE));

        aircraft = sortedCraft.poll();
        assertThat(aircraft.getType(), is(AircraftType.PASSENGER));
        assertThat(aircraft.getSize(), is(AircraftSize.SMALL));

        aircraft = sortedCraft.poll();
        assertThat(aircraft.getType(), is(AircraftType.PASSENGER));
        assertThat(aircraft.getSize(), is(AircraftSize.SMALL));

        aircraft = sortedCraft.poll();
        assertThat(aircraft.getType(), is(AircraftType.CARGO));
        assertThat(aircraft.getSize(), is(AircraftSize.LARGE));

        aircraft = sortedCraft.poll();
        assertThat(aircraft.getType(), is(AircraftType.CARGO));
        assertThat(aircraft.getSize(), is(AircraftSize.SMALL));
    }
}
