package com.muddworks.airtraffic.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created on 10/8/17.
 */
public class DepartingAircraftComparatorTest {

    private DepartingAircraftComparator comparator;

    @Before
    public void setUp() {
        this.comparator = new DepartingAircraftComparator();
    }

    @Test
    public void testSorting() {
        Set<DepartingAircraft> sortedCraft = new TreeSet<>(comparator);


        sortedCraft.add(new DepartingAircraft(new Aircraft("N2", AircraftType.PASSENGER, AircraftSize.SMALL)));
        sortedCraft.add(new DepartingAircraft(new Aircraft("N5", AircraftType.CARGO, AircraftSize.SMALL)));
        sortedCraft.add(new DepartingAircraft(new Aircraft("N4", AircraftType.CARGO, AircraftSize.LARGE)));
        sortedCraft.add(new DepartingAircraft(new Aircraft("N1", AircraftType.PASSENGER, AircraftSize.LARGE)));
        sortedCraft.add(new DepartingAircraft(new Aircraft("N3", AircraftType.PASSENGER, AircraftSize.SMALL)));

        List<DepartingAircraft> departingAircraftList = new ArrayList<>(sortedCraft);

        DepartingAircraft departingAircraft = departingAircraftList.get(0);
        assertThat(departingAircraft.getAircraft().getType(), is(AircraftType.PASSENGER));
        assertThat(departingAircraft.getAircraft().getSize(), is(AircraftSize.LARGE));
        assertThat(departingAircraft.getAircraft().getName(), is("N1"));

        departingAircraft = departingAircraftList.get(1);
        assertThat(departingAircraft.getAircraft().getType(), is(AircraftType.PASSENGER));
        assertThat(departingAircraft.getAircraft().getSize(), is(AircraftSize.SMALL));
        assertThat(departingAircraft.getAircraft().getName(), is("N2"));

        departingAircraft = departingAircraftList.get(2);
        assertThat(departingAircraft.getAircraft().getType(), is(AircraftType.PASSENGER));
        assertThat(departingAircraft.getAircraft().getSize(), is(AircraftSize.SMALL));
        assertThat(departingAircraft.getAircraft().getName(), is("N3"));

        departingAircraft = departingAircraftList.get(3);
        assertThat(departingAircraft.getAircraft().getType(), is(AircraftType.CARGO));
        assertThat(departingAircraft.getAircraft().getSize(), is(AircraftSize.LARGE));
        assertThat(departingAircraft.getAircraft().getName(), is("N4"));

        departingAircraft = departingAircraftList.get(4);
        assertThat(departingAircraft.getAircraft().getType(), is(AircraftType.CARGO));
        assertThat(departingAircraft.getAircraft().getSize(), is(AircraftSize.SMALL));
        assertThat(departingAircraft.getAircraft().getName(), is("N5"));
    }
}
